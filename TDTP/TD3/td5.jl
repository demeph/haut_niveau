#Matrice de permutation
M = [ 0 1 0 ; 1 0 0;0 0 1]
P = [ 0 1 0;1 0 1;0 0 1]
Q = [ 0 1 0; 3 0 0; 0 0 1]
R = [0 1 0;1 0 0 ; 1 0 0]
v = M[1,:]
println(countnz(v))

println([x for x in v if x != 0])
println([x for x in M[1,:] if x != 0])

function estPermutation(M)
    res = size(M)[1] == size(M)[2]
    for numligne in 1 :size(M)[1] # on peut ecrire size(M,1)
        res = res && [x for x in M[numligne,:] if x!=0 ] == [1]
    end
    for numLigne in 1:size(M,2)
        res = res && [x for x in M[:,numLigne] if x!=0] == [1]
    end
    return res
end

println(estPermutation(M)) #true
println(estPermutation(P)) #false
println(estPermutation(Q)) #false
println(estPermutation(R)) #false

X = [1:3...]

println(M*M*X)

#Matrice creuse
estPermutation(sparse(M))

println(sparse(M))

function estPerm(M::SparseMatrixCSC)
    res = size(M)[1] == size(M)[2]
    for numligne in 1 :size(M)[1] # on peut ecrire size(M,1)
        res = res && size(M[numligne,:],nzval)[1] == 1 && 1 in M[numligne,:]
    end
    for numLigne in 1:size(M,2)
        res = res && size(M[:,numLigne],nzval)[1] == 1 && 1 in M[:,numLigne]
    end
    return res
end

println(estPerm(sparse(M)))
