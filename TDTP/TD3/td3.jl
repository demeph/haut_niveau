#graphe & Algo djistra
mutable struct graphe
    nbsommets
    adj::Array{Real}
end

#creation d'un graphe
G = graphe(4,[-1 5 -1 2;-1 -1 7 -1;8 -1 -1 -1;-1 -1 4 -1])

function successeur(s,G)
    return [ i for i in 1:G.nbsommets if G.adj[s,i] != -1 ]
end

function preds(s,G)
    return [ i for i in 1:G.nbsommets if G.adj[i,s] != -1 ]
end

#plus court distance
function pcc(a,b,G)
    chs = copy(G.adj) #copie superficielle qui fonctionne avec les types simples, deepCopy pour les types complexes
    onfaitmieux = true
    while onfaitmieux
        onfaitmieux = false
        for deb in 1:G.nbsommets
            for but in successeur(deb,G)
                for pr in preds(deb,G)
                    newDist = chs[pr,deb]+chs[deb,but]
                    if chs[pr,but] == -1 || chs[pr,but] > newDist
                        chs[pr,but] = newDist
                        onfaitmieux = true
                    end
                end
            end
        end
    end
    return chs[a,b]
end


function plusCourtChemin(a,b,G)
    chs = copy(G.adj)
    onfaitmieux = true
    #creer un tableau avec un valeur par default, alternative [-1 for i in 1:G.nbsommets]
    lespreds = zeros(Integer,G.nbsommets,G.nbsommets)
    for src in 1:G.nbsommets
        for but in successeur(src,G)
            lespreds[src,but] = but
        end
    end
    while onfaitmieux
        onfaitmieux = false
        for deb in 1:G.nbsommets
            for but in 1:G.nbsommets
                for pr in successeur(deb,G)
                    newDist = chs[deb,pr]+chs[pr,but]
                    if chs[deb,but] > newDist
                        chs[deb,but] = newDist
                        lespreds[but] = pr
                        onfaitmieux = true
                    end
                end
            end
        end
    end
    return chs[a,b],lespreds
end
