# TD 1 - LPHN 

## Generateurs aleatoires

### 1- LCG

#### 1.a

```R
m = 2^31
random1 <- function (n) {
      ((7^5)*n+0) %% (m-1) 
}

random2 <- function (n) {
      (1103515245*x+12345)%%(m-1) 
}

lcg <- function(a,b,m) {
    return (function(x){(a*x+b)%%m})
}
// avec lcg, on a pas besoin definir diiferent fonction random, on peut juste generer la fonction

// par defaut random1
switchalea1 <- function(x0, nb,gen = random1) {
  t <- c(x0)
  for (i in 2:nb){
      t[i] <- gen(t[i-1])
  }
  return (t)
}


switchalea1(13,15,random1)
switchalea1(13,15)

//avec lcg
switchalea(13,15,lcg(7^5,15,m))

lcgs<-function(a,b,m) {return (function(x){(a*x+b)%%m})}
#avec lcg, on a pas besoin definir diiferent fonction random, on peut juste generer la fonction

# par defaut random1
switchalea1 <- function(x0, nb,gen) {
  t<-c(gen(x0))
  for (i in 2:nb){t[i]<-gen(t[i-1]) }
  return (t)
}

essai<-switchalea1(13,15,lcg(7^5,0,2^31-1))
hist(essai,xlab = "a",ylab="c")
plot(1:15,essai,xlab="a",ylab="essai")

```

### 2

#### 2.a

```R
#permet de genrer lancement  de deux des, avec remis cad on garde la valeur precedante
deuxdes <- sample(1:6,2,replace=TRUE)

cinqdes <- sample(1:6,5,replace=TRUE)

somme2des <- sum(sample(1:6,2,replace=TRUE))
centsomme2des <- sample(1:6,100,replace=TRUE)
hist(centsomme2des)
```

#### 2.b

```R
test <- function(n,p) { 
  desSommes <- sample(1:6,n,replace = TRUE,p)+sample(1:6,n,replace = TRUE,p)
  hist(desSommes)
}

test(100,c(0.3,0.15,0.05,0.05,0.15,0.3))
```

### 3 Monte-Carlo

##### 3.a

N points au hasard dont n point dans le disque

 alors $n/N = \pi /4$

