lcgs<-function(a,b,m) {return (function(x){(a*x+b)%%m})}
#avec lcg, on a pas besoin definir diiferent fonction random, on peut juste generer la fonction

# par defaut random1
switchalea1 <- function(x0, nb,gen) {
  t<-c(gen(x0))
  for (i in 2:nb){t[i]<-gen(t[i-1]) }
  return (t)
}

essai<-switchalea1(13,15,lcgs(7^5,0,2^31-1))
hist(essai,xlab = "a",ylab="c")
plot(1:15,essai,xlab="a",ylab="essai")

