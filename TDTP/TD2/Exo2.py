# brique 10x20x30 cm
import  time
def nbFacon(hauteur) :
    if hauteur == 10 :
        return 1
    elif hauteur == 20 :
        return 2
    else :
        return nbFacon(hauteur-10)+nbFacon(hauteur-20)


haut = 400

# t_avant = time.time()
# res = nbFacon(haut)
# t_apres = time.time()
#
# print("(classique) resultat pour le mur de hauter ",haut," : ",res," execute dans :",t_apres-t_avant,' s')

#deuxieme version
list = []
list = [-1]*(int)(haut/10)
def nbFacon2(hauteur) :
    if hauteur == 10 :
        return 1
    elif hauteur == 20 :
        return 2
    else :
        if list[(int)(((hauteur/10)-1)-1)] == -1 :
            nb1 = nbFacon2(hauteur-10)
            list[(int)(((hauteur/10)-1)-1)] = nb1
        else :
            nb1 = list[(int)(((hauteur/10)-1)-1)]
        if list[(int)(((hauteur/10)-2)-1)] == -1 :
            nb2 = nbFacon2(hauteur-20)
            list[(int)(((hauteur/10)-2)-1)] = nb2
        else :
            nb2 = list[(int)(((hauteur/10)-2)-1)]
        return nb1+nb2

t_avant = time.time()
res1 = nbFacon2(haut)
t_apres = time.time()


print("(consultation de tableaux) resultat pour le mur de hauter ",haut," : ",res1," execute dans :",t_apres-t_avant,' s')

#version 3
liste1 = []

def nbFaconRec(liste,hauteur, ind) :
    if ind == (int)((hauteur/10)- 1) :
        liste[ind] = liste[ind - 1] + liste[ind - 2]
        return liste[ind]
    else :
        liste[ind] = liste[ind - 2] + liste[ind - 1]
        return nbFaconRec(liste,hauteur,ind+1)



def nbFacon3(hauteur) :
    liste1 = [-1]*(int)(hauteur/10)
    liste1[0] = 1
    liste1[1] = 2
    return nbFaconRec(liste1,hauteur,2)

t_avant = time.time()
res2 = nbFacon3(haut)
t_apres = time.time()

print("(somme de indicde precedent) resultat pour le mur de hauter ",haut," : ",res2," execute dans :",t_apres-t_avant,' s')