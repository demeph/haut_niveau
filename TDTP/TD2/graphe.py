#liste d'aretes
G = [ (2,2),(2,4),(2,6),(2,6),(2,8),(3,3),(3,6),(5,5),(6,6),(7,7),(8,8),(4,4),(4,8) ]

#version 1, mais pas opti
def sommets(G) :
    S = []
    for couple in G :
        for sommet in couple :
            if sommet not in S :
                S.append(sommet)
    return S

#version 2
def sommets1(G) :
    return { ar[0] for ar in G }.union({ ar[1] for ar in G })

import functools
#version 3
def sommets2(G) :
   return set(functools.reduce(lambda a,b : a.union(b), map(lambda c : {c[0],c[1]},G)))

print(sommets(G))
print(sommets1(G))
print(sommets2(G))


# on prend les sommets dans laquelle on arrive a partir du sommet x, en verifiant que sommet depart est x
def voisin(x,G) :
    return { ar[1] for ar in G if ar[0] == x}

print(voisin(2,G))

def connexe(x,G) :
    # on note set({}) car les acolades ne sont pas specifique aux ensemble
    S = set({})
    voisinage = {x}
    while not(voisinage <= S)  :
        S = S.union(voisinage)
        voisinage = functools.reduce(lambda a,b : a.union(b), map(lambda x : voisin(x,G),S))
    return(S)

print(connexe(2,G))


#representation sommet-arretes
# liste des successeurs
# en utilisant dictionnait pour acceder un element avec son etiquette

def transformerGraphe(G) :
    return { s : voisin(s,G) for s in sommets1(G)}

print(transformerGraphe(G))

G1 = transformerGraphe(G)
def voisin2(x,G) :
    return G[x]

print(voisin2(2,G1))

def sommetListe(G) :
    return G.keys()

print(sommetListe(G1))

#avec cette implementation, la fonction connexe peut etre plus rapide, car la fonction voisin2 il accede plus rapidement les voisins que voisin