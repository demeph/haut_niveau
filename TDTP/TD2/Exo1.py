#Exercice 1
#1.a
# programmation classique
def genereMotif(x) :
    if x == 0 :
        return ['']
    else :
        tifs = genereMotif(x-1)
        res = []
        for moti in tifs :
            res = res + ['|'+moti]+['-'+moti]
        return res
#a = genereMotif(20)
#print(a)
#print(len(a))

#programmation python
def genereMotifopti(x) :
    if x == 0 :
        return ['']
    else :
        ssMotif = genereMotifopti(x-1)
        return ['|'+moti for moti in ssMotif] + ['--'+moti for moti in ssMotif]

#val = genereMotifopti(20)
#print(val)
#print(len(val))

# frises diff possibles : 2^x
#pour calculer les largeurs possible, on utilise map, pour calculet la longuere de chacun motif et puis on va utilser set qui permet d'afficher que des longueur uniques
taille = set(map(len,genereMotifopti(15)))
print(taille)

# 1.b
#les exmples :
#lgr = 0 --> pas de solution
#lgr = 1 --> |
#lgr = 2 --> || ou --
#lgr = 3 --> ||| ou --| ou |--

# pour 1 on sera oblige de mettre |
# pour 0 on met rien car il nous reste aucun longueur a exploiter

def motif(lgr) :
    if lgr == 0 :
        return ['']
    elif lgr == 1 :
        return ['|']
    else :
        return ['|'+moti for moti in motif(lgr-1)] + ['--'+moti for moti in motif(lgr-2)]


val = motif(5)

#pour calculer nb de motif pour les entiers succ
print([len(motif(k)) for k in range(10)])
# pour calculer nb de motif sans calculer motif, on pourrait utiliser fibo


def fib(n):
    a,b = 1,1
    for k in range(2,n) :
        a,b=b,a+b
    return b


#print(fib(25))


def nbpieces(motif) :
    if motif == '|' or motif == '--' :
        return 1
    elif motif[0] == '|' :
        return 1+ nbpieces(motif[1:])
    else :
        return 1+ nbpieces(motif[2:])

resu = motif(5)
print(resu)
print({nbpieces(motif) for motif in resu})

print(set(map(nbpieces,resu)))