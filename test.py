dd = open('toto','w')

dd.write('coucou')
dd.write('\nc\'est super je peux ecrire dans le fichier')

dd.close()
dd = open('toto', 'a')
dd.write('\nj\'ajoute le texte dans mon fichier')


liste1 = []

def nbFaconRec(liste,hauteur, ind) :
    if ind == (int)((hauteur/10)- 1) :
        liste[ind] = liste[ind - 1] + liste[ind - 2]
        return liste[ind]
    else :
        liste[ind] = liste[ind - 2] + liste[ind - 1]
        return nbFaconRec(liste,hauteur,ind+1)



def nbFacon3(hauteur) :
    liste1 = [-1]*(int)(hauteur/10)
    liste1[0] = 1
    liste1[1] = 2
    return nbFaconRec(liste1,hauteur,2)

print(nbFacon3(1000))