# LPHN - Julia 

## type des valeurs

### Integer

- entier sur 8 bit : Init8
  - ca ne change pas le typage dinamiquement, c a d Init8 ne passe pas en init!16
- Int $\in$ Real



### Chaine de caractere

- syntaxe : ""
  - echappement : """ 
- t[: ; 2] affiche toute la 2eme collone de la matrice
- t[1; : ] affiche la premiere ligne de la matrice en entier



### procedure

- push!(tb,3) : ca permet de modifier tb sans renvoyer le resultat d'ajout
- mut = push(tb,3) : ne modifie pas tb mains renvoyer l'ajout